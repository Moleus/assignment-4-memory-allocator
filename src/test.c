#define _DEFAULT_SOURCE

#include "test.h"
#include <assert.h>

void test_heap_allocation(FILE* output) {
    void* heap = init_test(output, "Heap allocation test_function", DEFAULT_HEAP_SIZE);
    struct block_header* block = allocate_block(BLOCK_CAPACITY);

    print_heap(output, heap, "Single block was allocated");

    assert(block != NULL);

    _free(block->contents);

    print_heap(output, heap, "Single block was freed");
    end_test(output, heap, DEFAULT_HEAP_SIZE);
}

void test_single_block_free(FILE* output) {
    const size_t second_block_size = BLOCK_CAPACITY;
    const size_t third_block_size = BLOCK_CAPACITY;

    void* heap = init_test(output, "Allocate 3 blocks. Free only one.", DEFAULT_HEAP_SIZE);
    print_heap(output, heap, "Second heap init");

    struct block_header* first_block = allocate_block(BLOCK_CAPACITY);
    struct block_header* second_block = allocate_block(second_block_size);
    struct block_header* third_block = allocate_block(third_block_size);
    assert(first_block->next == second_block);
    assert(second_block->next == third_block);
    print_heap(output, heap, "Three blocks was allocated");

    _free(third_block->contents);
    print_heap(output, heap, "After third block was freed");
    assert(first_block->is_free == false && first_block->next == second_block);
    assert(second_block->is_free == false && second_block->next == third_block);
    assert(third_block->is_free && third_block->next == NULL);
    end_test(output, heap, DEFAULT_HEAP_SIZE);
}

void test_multiple_block_free(FILE* output) {
    const size_t second_block_capacity = BLOCK_CAPACITY;
    const size_t third_block_capacity = BLOCK_CAPACITY;

    void* heap = init_test(output, "Allocate 3 blocks. Free the first and the second.", DEFAULT_HEAP_SIZE);

    struct block_header* first_block = allocate_block(BLOCK_CAPACITY);
    struct block_header* second_block = allocate_block(second_block_capacity);
    struct block_header* third_block = allocate_block(third_block_capacity);

    // free order matters
    _free(second_block->contents);
    _free(first_block->contents);

    print_heap(output, heap, "After the first and the second blocks were freed");
    assert(first_block->is_free && first_block->next == third_block);

    assert(size_from_capacity(first_block->capacity).bytes == block_capacity_to_size(BLOCK_CAPACITY) +
                                                              block_capacity_to_size(second_block_capacity));

    assert(second_block->is_free && second_block->next == third_block);
    end_test(output, heap, DEFAULT_HEAP_SIZE);
}

void test_region_extends_following(FILE* output) {
    void* heap = init_test(output, "Heap should grow with the following region.", HEAP_MIN_SIZE);
    print_heap(output, heap, "Heap with minimal size.");

    struct block_header* first_block = allocate_block(REGION_MIN_SIZE + BLOCK_CAPACITY);
    print_heap(output, heap, "First block filled the full region + BLOCK_CAPACITY.");

    struct block_header* second_block = allocate_block(BLOCK_CAPACITY);
    assert(first_block->next == second_block);
    assert((uint8_t*) first_block + size_from_capacity(first_block->capacity).bytes == (uint8_t*) second_block);

    end_test(output, heap, DEFAULT_HEAP_SIZE + BLOCK_CAPACITY);
}

void test_region_extends_not_following(FILE* output) {
    void* heap = init_test(output, "Heap should grow but the next region is filled.", HEAP_MIN_SIZE);

    void* _mapped_gap = mmap(heap + REGION_MIN_SIZE, BLOCK_CAPACITY, PROT_READ | PROT_WRITE,
                             MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert(_mapped_gap != MAP_FAILED);

    struct block_header* second_block = allocate_block(REGION_MIN_SIZE + BLOCK_CAPACITY);
    print_heap(output, heap, "Heap extended with the not following region.");

    struct block_header* empty_block = (struct block_header*) heap;
    assert(empty_block->is_free);
    assert(empty_block->next == second_block);

    assert((uint8_t*) heap + REGION_MIN_SIZE + BLOCK_CAPACITY < (uint8_t*) second_block);

    _free(second_block->contents);
    print_heap(output, heap, "After block free and merge:");
    munmap(heap, REGION_MIN_SIZE);
    end_test(output, second_block, size_from_capacity(second_block->capacity).bytes);
}

