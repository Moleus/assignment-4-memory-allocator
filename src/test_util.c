#include "mem.h"
#include "mem_internals.h"
#include "test_util.h"
#include "util.h"
#include <assert.h>

static void print_start(FILE* output, const char* test_name) {
    fprintf(output, "----- Start test [%s] -----\n", test_name);
}

static void print_finish(FILE* output) {
    fputs("----- Test passed -----\n", output);
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*) contents) - offsetof(struct block_header, contents));
}

void print_heap(FILE* output, void* heap, const char* description) {
    fprintf(output, "%s\n", description);
    debug_heap(output, heap);
}

void* init_test(FILE* output, const char* test_name, size_t heap_size) {
    print_start(output, test_name);
    void* new_heap = heap_init(heap_size);
    assert(new_heap != NULL);
    return new_heap;
}

static int clear_heap(void* heap, size_t bytes) {
    return munmap(heap, size_from_capacity((block_capacity){.bytes = bytes}).bytes);
}

void end_test(FILE* output, void* heap, size_t clear_bytes) {
    int clear_status = clear_heap(heap, clear_bytes);
    assert(clear_status != -1);
    print_finish(output);
}

struct block_header* allocate_block(size_t capacity) {
    void* block_contents = _malloc(capacity);
    assert(block_contents != NULL);
    struct block_header* header = block_get_header(block_contents);
    assert(header->capacity.bytes == capacity);
    assert(header->is_free == false);
    return header;
}

size_t block_capacity_to_size(size_t bytes) {
    return size_from_capacity((block_capacity) {.bytes = bytes}).bytes;
}