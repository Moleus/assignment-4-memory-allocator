#include "test.h"

#define DEBUG_FILE stdout

int main() {
    for (int i = 0; i < TESTS_COUNT; i++) {
        defined_tests[i](DEBUG_FILE);
    }
    fflush(DEBUG_FILE);
    return 0;
}

