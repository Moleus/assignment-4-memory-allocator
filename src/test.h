#pragma once

#include "mem.h"
#include "stdbool.h"
#include "test_util.h"

#define TESTS_COUNT 5

typedef void (test_function)(FILE* output);

void test_heap_allocation(FILE* output);

void test_single_block_free(FILE* output);

void test_multiple_block_free(FILE* output);

void test_region_extends_following(FILE* output);

void test_region_extends_not_following(FILE* output);

static test_function* const defined_tests[TESTS_COUNT] = {
        test_heap_allocation,
        test_single_block_free,
        test_multiple_block_free,
        test_region_extends_following,
        test_region_extends_not_following
};