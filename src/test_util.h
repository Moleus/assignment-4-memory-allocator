#pragma once

#include "mem_internals.h"
#include "stdio.h"
#include "stdlib.h"
#include "util.h"

#define DEFAULT_HEAP_SIZE (4096)
#define HEAP_MIN_SIZE 1
#define BLOCK_CAPACITY 1024

void* init_test(FILE* output, const char* test_name, size_t heap_size);

void end_test(FILE* output, void* heap, size_t clear_bytes);

void print_heap(FILE* output, void* heap, const char* description);

struct block_header* allocate_block(size_t capacity);

size_t block_capacity_to_size(size_t bytes);